$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "api_providers/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "api_providers"
  s.version     = ApiProviders::VERSION
  s.authors     = ["Gabriela"]
  s.email       = ["gabagrant@gmail.com"]
  s.homepage    = ""
  s.summary     = "Summary of ApiProviders."
  s.description = "Description of ApiProviders."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "< 5.1", ">= 5.0.6"
  s.add_dependency "httparty"

  s.add_development_dependency "sqlite3"
end
