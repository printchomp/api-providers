require 'api_providers/category'
namespace 'export' do

  task :products => :environment do

    ids = [14, 16, 21, 22, 28, 31, 32, 33, 41, 48]

    base_options = ['size', 'coating','stock', 'turnaround', 'runsize', 'colorspec']
    ApiProviders::Category.find(ids).each do |category|
      other_options = ApiProviders::ProductVariant.includes(:options).joins(:product)
        .where(api_providers_products: { category_id: category.id }).all
        .reject { |var| var.product.blank? || var.product.discounted }
        .map(&:options).flatten.map(&:property).flatten.uniq.pluck(:slug)
      all_options = base_options + ( other_options - base_options )
      CSV.open("#{category.name.downcase.gsub(/\s/, '_')}.csv", 'w', write_headers: true, headers: ['Name', 'Code'] + all_options.map(&:capitalize)) do |csv|
          ApiProviders::ProductVariant.joins(:product).where(api_providers_products: { category_id: category.id }).each do |variant|
          next unless (variant.product && !variant.product.discounted)
          options = variant.options.joins(:property)

          csv << [variant.product.name,
                  variant.product.code ] + (all_options.collect { |name| options.where('api_providers_properties.slug = ?', name).pluck(:name).join(', ') })

        end
      end
    end
  end
end

namespace :cimpress_products do
  task import: :environment do
    skus = {quantity: [], stock: [], colorspec: []}
    xlsx = Roo::Spreadsheet.open('/Users/Gabz/api_providers/test/dummy/lib/tasks/Printchomp-RateCard.xlsx')

    xlsx.each_row_streaming(offset: 1, pad_cells: true) do |row|
      quantity = row[3].to_s
      stock = row[4].to_s
      colorspec = row[5].to_s

      skus[:quantity] <<  quantity unless skus[:quantity].include?(quantity)
      skus[:stock] << stock unless skus[:stock].include?(stock)
      skus[:colorspec] << colorspec unless skus[:colorspec].include?(colorspec)
    end

    puts skus
  end
end

