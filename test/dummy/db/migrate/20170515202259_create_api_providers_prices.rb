class CreateApiProvidersPrices < ActiveRecord::Migration[5.0]
  def change
    create_table :api_providers_prices do |t|
      t.integer :product_id
      t.index :product_id
      t.decimal :amount, null: false, default: 0.0
      t.string :option_sku
      t.string :runsize_sku
      t.string :colorspec_sku
      t.integer :min_quantity
      t.integer :max_quantity
      t.decimal :per_quantity, null: false, default: 0.0

      t.timestamps
    end
  end
end
