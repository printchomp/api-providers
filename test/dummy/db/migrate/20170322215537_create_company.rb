class CreateCompany < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :slug
      t.string :provider_slug
    end
  end
end
