# ApiProviders

4Over API documentation: https://api-users.4over.com/?page_id=39
Cimpress API documentation: https://docs.cimpress.io//print_fulfillment/index.html

## Usage

### Set your provider class

```
  class Company < ActiveRecord::Base
    include ApiProviders::Provider
  end
```

See `lib/api_providers/provider.rb` for list of available methods

### Building your custom product

The product matching code is not very complex. If you're going to
combine multiple products into one, you can only have *ONE* property
that is different among them. For e.g., if you have 5 kinds of business
cards with different kind of coatings (AQ, UV etc, etc), all other
options (colorspec, runsize, size etc) *MUST* be the same.

Otherwise,the wrong product might be sent to the printer, or no product found at
all and an unexpected error may occur.

### Terminology
`codes` - These are the unique identifiers for the provider's products,
e.g. 91efff4d-de64-410a-8794-fc547022a7f2 for 4Over or VIP-47668 for
Cimpress. This is mapped back to the sku field in the provider models.

`skus` - These are the unique identifiers for the provider's product
options, e.g.4d0c12ba-13c5-4851-ac98-d5908a8da60b (No Spot UV) for
4Over. Cimpress does not have unique identifiers for their options, so
one will be generated for each option on import. There are a few edges
cases where 4Over is lacking one as well (e.g. colors for shirts). Those will need to be manually
added for now.

`destination` - The address of the person who ordered

`delivery` - This is the shipping information returned by the provider
along with the shipping quote. This is required.

### Getting a shipping quote

```
params = { destination: {
             name: 'John Smith'
             email: 'test@test.com',
             phone: '1234567890',
             address1: '123 Test St',
             address2: '',
             city: 'Toronto',
             state: 'ON,
             zipcode: '',
             country: 'CA'
          },
          skus: ['120lb-matte'],
          codes: ['VIP-47668']
        }

company.shipping_quote(params)
```

### Sending an order to Cimpress

```
params =   { id: '1',
             total: '144.89',
             items: [
               {
                 id: '1',
                 name: 'Custom Business Cards',
                 sku: 'VIP-47668',
                 quantity: 100,
                 files: [ 'http://foo.com' ]
               }
             ],
             destination: {
               name: 'John Smith'
               email: 'test@test.com',
               phone: '1234567890',
               address1: '123 Test St',
               address2: '',
               city: 'Toronto',
               state: 'ON,
               zipcode: '',
               country: 'CA'
             },
             delivery: {
               id: '11'
             }
           }

company.submit_order(params)
```

### Sending an order to 4Over

```
params =   { id: '1',
              items: [
              {
                product_uuid: '91efff4d-de64-410a-8794-fc547022a7f2',
                runsize_uuid: '6237a36b-b046-4ef6-8fed-6cb9c22a5ece',
                colorspec_uuid: '32d3c223-f82c-492b-b915-ba065a00862f',
                turnaroundtime_uuid: '50979118-b3a2-4556-9f46-d1da268f2354'
                option_uuids: ['12b85fea-3513-46fd-932f-b2b9c1c17dbf'],
                files: ['front.jpg', 'back.jpg'],
              }
            ],
            origin: {
              company: 'Printchomp'
              first_name: 'Joseph',
              last_name: 'Puopolo',
              email: 'joseph@printchomp.com',
              phone: '1234567890'
              address1: '123 Test St',
              address2: ''
              city: 'Kitchener',
              state: 'ON'
              zipcode: '',
              country: 'CA',
            },
            destination: {
              company: 'Johnny Smith',
              email: 'test@test.com',
              phone: '1234567890',
              address1: '123 Test St',
              address2: '',
              city: 'Toronto',
              state: 'ON,
              zipcode: '',
              country: 'CA'
            },
            delivery: {
              id: '11',
              name: 'UPS Standard',
              facility: 'TOR'
            }
          }

company.submit_order(params)
```

## Installation
```ruby
gem 'api_providers'
```
```bash
bundle install
rails generate api_providers
rake db:migrate
```

Or install it yourself as:
```bash
$ gem install api_providers
```

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
