require 'rails/generators'
require 'rails/generators/active_record'

class ApiProvidersGenerator < ActiveRecord::Generators::Base
  argument :name, type: :string, default: 'api_providers'

  source_root File.expand_path('../../api_providers', __FILE__)

  def copy_files
    migration_template 'migration.rb', 'db/migrate/create_api_providers_tables.rb'
  end
end
