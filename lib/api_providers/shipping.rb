require 'api_providers/product'
module ApiProviders
  class Shipping
    def initialize(provider, order)
      @provider = provider
      @skus = order.delete(:skus)
      @codes = order.delete(:codes)
      @order = order
    end

    def quote
      product = Product.fetch( @codes, @skus, @provider)
      order = @order.merge(product: product)
      ("ApiProviders::#{@provider.slug.classify}::Shipping").constantize.new(order).quote
    end
  end
end
