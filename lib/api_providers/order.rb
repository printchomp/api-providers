module ApiProviders
  class Order

    def initialize(provider, params)
      @provider = provider
      @items = params.delete(:items)
      @details = params
    end

    def submit
      products = Product.fetch_all(@items, @provider)
      order = @details.merge( { items: products } )

      ("ApiProviders::#{@provider.slug.classify}::Order").constantize.new(order).submit
    end


  end
end
