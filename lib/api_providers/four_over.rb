require 'api_providers/four_over/base'
require 'api_providers/four_over/category'
require 'api_providers/four_over/product'
require 'api_providers/four_over/option_group'
require 'api_providers/four_over/option'
require 'api_providers/four_over/order'
require 'api_providers/four_over/address'
require 'api_providers/four_over/shipping'

module ApiProviders
  module FourOver

    class << self
      attr_writer :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.configure
      yield(configuration)
    end

    def self.status
      "Status Unavailable"
    end

  end
end
