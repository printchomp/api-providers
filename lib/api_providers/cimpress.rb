require 'api_providers/cimpress/base'
require 'api_providers/cimpress/category'
require 'api_providers/cimpress/property'
require 'api_providers/cimpress/option'
require 'api_providers/cimpress/product'
require 'api_providers/cimpress/address'
require 'api_providers/cimpress/shipping'
require 'api_providers/cimpress/order'

module ApiProviders
  module Cimpress

    class << self
      attr_writer :configuration
    end

    def self.configuration
      @configuration ||= Configuration.new
    end

    def self.configure
      yield(configuration)
    end

    def self.status
      "Status Unavailable"
    end

  end
end
