require 'api_providers/property'
require 'api_providers/option'

module ApiProviders
  class ProductVariant < ActiveRecord::Base
    has_and_belongs_to_many :options, class_name: 'ApiProviders::Option'
    belongs_to :product

    scope :by_product_codes, ->(codes) { joins(:product).where(api_providers_products: { code: codes }) }

    class << self
      def filter(skus)
        where("NOT EXISTS (SELECT * FROM api_providers_options
              WHERE NOT EXISTS (SELECT * FROM api_providers_options_product_variants
              WHERE api_providers_options_product_variants.option_id = api_providers_options.id
              AND api_providers_options_product_variants.product_variant_id = api_providers_product_variants.id)
              AND api_providers_options.sku IN (?))", skus) #.limit(1)

      end

      def find_by_sku(sku)
        joins(:options).where(api_providers_options: { sku: sku })
      end

      def find_closest_match(variants, skus)
        return variants.first if variants.length == 1
        variants.min { |variant| variant.options.non_dependant.without_skus(skus).length <=> variant.options.non_dependant.without_skus(skus).length }
      end
    end

  end
end
