module ApiProviders
  class Property < ActiveRecord::Base

    has_many :options, class_name: 'ApiProviders::Option'
    belongs_to :provider, foreign_key: :provider_id

    def self.import(provider_id)
      FourOver::OptionGroup.fetch_all.each do |group|
        property = self.find_or_initialize_by(sku: group.sku)
        property.update_attributes(name: group.name, sku: group.sku, slug: group.slug, provider_id: provider_id)
      end
    end
  end
end
