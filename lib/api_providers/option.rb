module ApiProviders
  class Option < ActiveRecord::Base

    has_and_belongs_to_many :product_variants, class_name: 'ApiProviders::ProductVariant'
    belongs_to :property, class_name: 'ApiProviders::Property'

    validates :sku, uniqueness: true

    scope :non_dependant, ->() { joins(:property).where.not(api_providers_properties: { slug: ['runsize', 'colorspec', 'turnaround'] } ) }
    scope :dependant, ->() { joins(:property).where(api_providers_properties: { slug: ['runsize', 'colorspec', 'turnaround'] } ) }
    scope :additional, ->() { joins(:property).where.not(api_providers_properties: { slug: %w(stock size coating runsize colorspec turnaround) } ) }
    scope :by_property_type, -> (type) { joins(:property).where(api_providers_properties: { slug: type }) }
    scope :non_custom, -> () { joins(:property).where(api_providers_properties: { custom: false }) }

    scope :without_skus, ->(skus) { where.not(sku: skus) }

    scope :search_by_name, ->(name) { if ActiveRecord::Base.connection.adapter_name == 'PostgreSQL'
                                        where('api_providers_options.name ILIKE ?', "%#{name}%")
                                      else
                                        where('api_providers_options.name LIKE ?', "%#{name}%")
                                      end
                                    }
    scope :filter_by_product, ->(product_ids) { joins(:product_variants).where(api_providers_product_variants: { product_id: product_ids }).distinct }

    class << self
      def find_by_skus(skus)
        properties = {}

        non_custom.where(api_providers_options: { sku: skus }).each do |option|
          properties[option.property.slug] = option.sku
        end

        properties
      end

      def runsizes
        by_property_type('runsize')
      end

      def colorspecs
        by_property_type('colorspec')
      end

      def turnarounds
        by_property_type('turnaround')
      end

      def minimum_runsize
        runsizes.group('api_providers_options.id').order('MIN(CAST(api_providers_options.name AS integer)) ASC').limit(1).first
      end
    end
  end
end
