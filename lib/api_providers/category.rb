module ApiProviders
  class Category < ActiveRecord::Base

    has_many :products, class_name: 'ApiProviders::Product'

    def self.import(provider)
      case provider.slug
      when 'four_over'
        Property.import(provider.id)

        FourOver::Category.fetch_all.each do |fo_category|
          next if self.exists?(sku: fo_category.sku)

          self.create!(name: fo_category.name, sku: fo_category.sku, provider_id: provider.id)
        end

      else
        self.create!(name: 'General', provider_id: provider.id)
      end
    end

    def create_products(provider)
      category = ("ApiProviders::#{provider.slug.classify}::Category").constantize.new(name, sku: self.try(:sku))

      category.products.each do |provider_product|
        next if provider_product.discounted? || products.exists?(sku: provider_product.sku)

        products.create!(name: provider_product.name, sku: provider_product.sku, code: provider_product.code)
      end
    end

  end
end
