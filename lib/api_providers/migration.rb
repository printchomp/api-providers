class CreateApiProvidersTables < ActiveRecord::Migration
  def change
    create_table :api_providers_categories do |t|
      t.string :name
      t.string :sku
      t.integer :provider_id
      t.index :provider_id

      t.timestamps
    end

    create_table :api_providers_prices do |t|
      t.integer :product_id
      t.index :product_id
      t.decimal :amount, precision: 8, scale: 2
      t.string :option_sku
      t.string :runsize_sku
      t.string :colorspec_sku
      t.integer :min_quantity
      t.integer :max_quantity
      t.integer :increments_of
      t.decimal :per_quantity, precision: 8, scale: 2

      t.timestamps
    end

    create_table :api_providers_products do |t|
      t.string :name
      t.string :sku
      t.string :code
      t.string :region
      t.integer :category_id
      t.index :category_id

      t.timestamps
    end

    create_table :api_providers_properties do |t|
      t.string :name
      t.string :sku
      t.string :slug
      t.integer :provider_id
      t.index :provider_id
      t.boolean :custom, null: false, default: false

      t.timestamps
    end

    create_table :api_providers_options do |t|
      t.string :name
      t.string :sku, index: true
      t.integer :property_id
      t.index :property_id

      t.timestamps
    end

    create_table :api_providers_product_variants do |t|
      t.integer :product_id
      t.index :product_id
    end

    create_table :api_providers_options_product_variants do |t|
      t.integer :product_variant_id
      t.integer :option_id
      t.index [:product_variant_id, :option_id], name: 'idx_ap_options_products_on_product_variant_id_and_option_id'
    end
  end
end
