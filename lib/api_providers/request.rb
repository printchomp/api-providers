module ApiProviders
  module Request

    def self.build(method, url, options={})
      @retries = 0

      begin
        HTTParty.send(method, url, options)
      rescue Net::ReadTimeout, Errno::ECONNRESET  => e
        Rails.logger.info e
        @retries += 1
        sleep 5
        retry if @retries < 3
      end
    end
  end
end
