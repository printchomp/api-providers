module ApiProviders
  module Provider

    extend ActiveSupport::Concern

    included do
      validates :provider_slug, uniqueness: true, inclusion: { in: %w(four_over cimpress) }, allow_blank: true

      has_many :api_categories, class_name: 'ApiProviders::Category', foreign_key: :provider_id
      has_many :api_properties, class_name: 'ApiProviders::Property', foreign_key: :provider_id
    end

    def slug
      self.provider_slug
    end

    def cimpress?
      provider_slug == 'cimpress'
    end

    def four_over?
      provider_slug == 'four_over'
    end

    def provider_status
      ("ApiProviders::#{slug.classify}").constantize.status
    end

    #Lists all options for provider products
    def api_options
      ApiProviders::Option.joins(:property).where(api_providers_properties: { provider_id: self.id })
    end

    #import categories for provider products
    def setup_for_provider_products
      ApiProviders::Category.import(self)
    end

    #import products for category from providers
    def import_provider_products(category)
      category.create_products(self)
    end

    #import properties and property options for products from providers based on product codes (e.g. 'VIP
    def import_products_information(codes)
      ApiProviders::Product.create_products_information(self, codes)
    end

    #Send order to provider
    def submit_order(params)
      ApiProviders::Order.new(self, params).submit
    end

    #Get shipping quote from provider
    def shipping_quote(params)
      ApiProviders::Shipping.new(self, params).quote
    end

    #Get product cost from provider
    def product_quote(skus, codes)
      if codes.length == 1
        product_ids = Product.where(code: codes).pluck(:id)
      else
        product_ids = Product.find_variants_by_skus(skus, codes).pluck(:product_id).uniq
      end

      Product.quote(self, skus, product_ids)
    end

    #Get starting price from provider
    def starting_price_for(codes, skus)
      options =     Option.where(sku: skus)
      product_ids = Product.where(code: codes).pluck(:id)

      if four_over?
        Price.where({ runsize_sku:   options.runsizes.pluck(:sku),
                      colorspec_sku: options.colorspecs.pluck(:sku),
                      option_sku:    options.turnarounds.pluck(:sku),
                      product_id:    product_ids }.reject { |k,v| v.blank? }
                   ).minimum(:amount).to_f
      else
        min_runsize = options.minimum_runsize.try(:name).try(:to_i) || 1
        Price.by_quantity(min_runsize).minimum(:amount).to_f
      end
    end
  end
end
