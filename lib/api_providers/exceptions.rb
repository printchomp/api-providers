module ApiProviders

  class ApiProvidersError < ::StandardError; end

  class UnexpectedError < ApiProvidersError; end
end
