module ApiProviders
  class Price < ActiveRecord::Base

    belongs_to :product, class_name: 'ApiProviders::Product'

    validates_presence_of :amount

    #Alias for 4Over api attributes
    alias_attribute :price, :amount
    alias_attribute :endqty, :max_quantity
    alias_attribute :startqty, :min_quantity
    alias_attribute :price_per_qty, :per_quantity
    alias_attribute :qty, :increments_of

    alias_attribute :option_uuid, :option_sku
    alias_attribute :runsize_uuid, :runsize_sku
    alias_attribute :colorspec_uuid, :colorspec_sku

    #Alias for Cimpress api attributes
    alias_attribute :WholesalePrice, :amount
    alias_attribute :MinQuantity, :min_quantity
    alias_attribute :MaxQuantity, :max_quantity

    scope :by_quantity, ->(quantity) { where('? BETWEEN min_quantity AND max_quantity', quantity) }
    scope :additional, ->() { where(colorspec_sku: nil, runsize_sku: nil) }

    class << self

      def additional_options_price(skus, quantity)
        total = 0

        prices = additional.where('option_sku IN (?) AND ((? BETWEEN min_quantity AND max_quantity) OR min_quantity IS NULL)', skus, quantity)
        prices.each do |price|
          if price.per_quantity.present? && (price.increments_of.present? && !price.increments_of.zero?)
            # if price per quantity is present, divide quantity by the increment before multiplying by the per quantity price, then add the flat rate
            total += ( (price.per_quantity * (quantity.to_f / price.increments_of)) + price.amount )
          else
            total += price.amount
          end
        end

        total
      end

    end
  end

end
