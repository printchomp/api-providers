require 'api_providers/product_variant'

module ApiProviders
  class Product < ActiveRecord::Base

    has_many :variants, class_name: 'ApiProviders::ProductVariant', dependent: :destroy
    has_many :options, through: :variants
    has_many :prices, class_name: 'ApiProviders::Price', dependent: :destroy
    belongs_to :category, class_name: 'ApiProviders::Category'


    class << self
      def find_variants_by_skus(skus, codes)
        ProductVariant.by_product_codes(codes).filter(skus)
      end

      def fetch_all(items, provider)
        @provider = provider

        products = []
        items.each do |item|
          product = fetch(item.delete(:codes), item.delete(:skus), provider)
          products << item.merge(product)
        end

        products
      end


      def fetch(codes, skus, provider)
        matches = find_variants_by_skus(skus, codes)
        #Find product variant that's the closest match based on provided skus
        product = ProductVariant.find_closest_match(matches, skus).try(:product)

        convert_skus(product, skus, provider)
      end

      def set_prices(provider, product_code=nil)
        case provider.provider_slug
        when 'four_over'
          blacklist = ApiProviders::Property.where(slug: %w(colorspec runsize stock size coating)).pluck(:sku)
          product = ApiProviders::Product.find_by(code: product_code)

          return if product.blank?

          provider_product = ApiProviders::FourOver::Product.new(product.name, product.sku, product.code)

          provider_product.properties.each do |property|
            next if blacklist.include? property.sku

            if property.turnaround?
              prices = provider_product.prices_with_turnaround(property.options)
            else
              prices = []
              property.options.each {  |option| prices += option.prices.data }
            end

            prices.each { |price_data| product.create_or_update_price(price_data.except('price'), price_data['price']) }
          end
        when 'cimpress'
          ApiProviders::Cimpress::Product.base_prices.data.each do |sku, prices|
            product = ApiProviders::Product.find_by_sku(sku)
            prices.each { |price_data| product.create_or_update_price(price_data.except('WholesalePrice'), price_data['WholesalePrice']) } if product.present?
          end
        end
      end

      def quote(provider, skus, product_ids)
        products_prices = Price.where(product_id: product_ids)
        options         = ApiProviders::Option.non_custom.where(sku: skus)
        quantity        = options.minimum_runsize.try(:name).try(:to_i)
        other_prices    = 0

        case provider.slug
        when 'four_over'
          query =  { option_sku:    options.turnarounds.pluck(:sku),
                     colorspec_sku: options.colorspecs.pluck(:sku),
                     runsize_sku:   options.runsizes.pluck(:sku),
                     product_id:    product_ids
                   }.reject { |k,v| v.blank? }

          base_price   = products_prices.where.not(colorspec_sku: nil, runsize_sku: nil).where(query).minimum(:amount)
          other_prices = products_prices.additional_options_price(options.additional.pluck(:sku), quantity)
        when 'cimpress'
          base_price = products_prices.by_quantity(quantity).minimum(:amount)
        end

        (base_price.to_f + other_prices.to_f).round(2)
      end

      def create_products_information(provider, codes)
        self.where(code: codes).each do |product|
          puts "    Product: NAME - #{product.name}, SKU - #{product.sku}, CODE - #{product.code}"
          provider_product = ("ApiProviders::#{provider.slug.classify}::Product").constantize.new(product.name, product.sku, product.code)
          provider_product.properties.each { |provider_property| create_properties(provider, provider_property) }

          puts "    Product properties: #{provider_product.properties.length}"

          provider_product.variant_skus.each do |skus|
            variant = ProductVariant.create!(product: product)
            variant.options << Option.where(sku: skus)
          end

          sleep 1
        end
      end

      private
        def create_properties(provider, provider_property)
          if provider.slug == 'four_over'
            property = Property.find_by_sku(provider_property.sku)
          else
            property = Property.where(slug: provider_property.slug, provider_id: provider.id).first
            property = Property.create(name: provider_property.name, slug: provider_property.slug, provider_id: provider.id) if property.blank?
          end

          provider_property.options.each do |provider_option|
            property.options.create!(name: provider_option.name, sku: provider_option.sku) unless property.options.where(sku: provider_option.sku).exists?
          end
        end

        def convert_skus(product, skus, provider)
          options = ApiProviders::Option.find_by_skus(skus)

          case provider.slug

          when 'four_over'
              { product_uuid:        product.sku,
                colorspec_uuid:      options.delete('colorspec'),
                runsize_uuid:        options.delete('runsize'),
                turnaroundtime_uuid: options.delete('turnaround'),
                option_uuids:        options.values
              }
          when 'cimpress'
            { sku: product.sku,
              quantity: options.delete('runsize').to_i
            }
          end
        end
    end

    def create_or_update_price(data, price)
      product_price = self.prices.find_or_initialize_by(data)
      product_price.update_attribute(:price, price.to_f)
    end
  end
end
