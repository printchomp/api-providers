module ApiProviders
  class Response

    attr_reader :provider_order_ids, :shipping_options, :data

    def initialize(response_code, messages, params={})
      @success = (response_code == 200)
      @messages = messages
      @provider_order_ids = params[:provider_order_ids]
      @shipping_options = params[:shipping_options]
      @data = params[:data]
    end

    def success?
      @success
    end

    def messages
      @messages
    end

    def data
      @data
    end
  end
end
