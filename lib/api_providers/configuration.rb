module ApiProviders
  class Configuration
    attr_accessor :four_over_key
    attr_accessor :four_over_get_sig
    attr_accessor :four_over_post_sig
    attr_accessor :four_over_delete_sig
    attr_accessor :mode
    attr_accessor :four_over_payment_uuid
    attr_accessor :credit_card

    attr_accessor :cimpress_client_id
    attr_accessor :cimpress_email
    attr_accessor :cimpress_password
  end
end
