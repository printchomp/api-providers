module ApiProviders
  class Configuration
    attr_accessor :key
    attr_accessor :get_signature
    attr_accessor :post_signature
    attr_accessor :delete_signature
    attr_accessor :mode
    attr_accessor :payment_uuid
    attr_accessor :credit_card
  end

end
