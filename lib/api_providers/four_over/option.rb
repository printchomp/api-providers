module ApiProviders
  module FourOver
    class Option < Base

      def initialize(option)
        @option = option
      end

      attr_reader :option

      def name
        if @option["option_name"].blank? || (runsize_sku.present? && colorspec_sku.present?)
          description
        else
          @option["option_name"]
        end
      end

      def description
        @option["option_description"]
      end

      def sku
        @option["option_uuid"]
      end

      def runsize
        @option["runsize"]
      end

      def runsize_sku
        @option["runsize_uuid"]
      end

      def colorspec
        @option["colorspec"]
      end

      def colorspec_sku
        @option["colorspec_uuid"]
      end

      def self.one_sided_colorspec
        '171edaa0-1c7f-47ac-a15b-b9db4604c256'
      end

      def dependant_skus
        [sku, runsize_sku, colorspec_sku]
      end

      def prices
        response = send_get_request(@option['option_prices'])

        response_fields = ['price', 'option_uuid', 'runsize_uuid', 'colorspec_uuid', 'startqty', 'endqty', 'price_per_qty', 'qty']
        structured_data = response['entities'].collect { |entity| { 'option_uuid' => sku }.merge(entity.extract!(*response_fields)) }
        Response.new(response.code, response['message'], { data: structured_data || [] })
      end
    end
  end
end
