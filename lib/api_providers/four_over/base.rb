require 'api_providers/request'

module ApiProviders
  module FourOver
    class Base

      SANDBOX_URL = "https://sandbox-api.4over.com"
      PRODUCTION_URL = "https://api.4over.com"

      class << self
        def config
          ApiProviders.configuration
        end

        def test_mode?
          config.mode != :production
        end
      end

      protected

      # Options example { max: 1000, offset: 0 }

      def send_get_request(path, options={})
        query = { apikey: config.four_over_key, signature: config.four_over_get_sig, max: 1000, offset: 0 }.merge(options)
        Request.build(:get, full_url(path), query: query )
      end

      def send_post_request(path, body)
        headers = { "Authorization" => "API #{config.four_over_key}:#{config.four_over_post_sig}"}
        Request.build(:post, full_url(path), { headers: headers, body: body.to_json })
      end

      private

      def full_url(path)
        mode_url = self.class.test_mode? ? SANDBOX_URL : PRODUCTION_URL
        path = path.gsub(Regexp.new(mode_url), '') if path.match(mode_url)
        "#{mode_url}#{path}"
      end

      def config
        self.class.config
      end
    end
  end
end
