module ApiProviders
  module FourOver
    class Product < Base

      def initialize(name, sku, code)
        @name = name
        @sku = sku
        @code = code
      end

      attr_reader :name, :sku, :code

      def properties
        response = send_get_request("/printproducts/products/#{@sku}/optiongroups")["entities"]
        response.present? ? response.collect { |group| ApiProviders::FourOver::OptionGroup.new(group) } : []
      end

      def variant_skus
        @variants = []
        indexed = properties.index_by(&:slug).slice!('colorspec', 'runsize')
        turnaround_options = indexed.delete('turnaround')&.options&.group_by(&:sku) || []
        indexed_options = indexed.values.map(&:options).flatten.map(&:sku)

        turnaround_options.each do |sku, turnaround_option|
          @variants << (turnaround_option.map(&:dependant_skus).flatten.uniq + indexed_options)
        end

        @variants
      end

      def quote(options)
        query = { product_uuid: @sku }

        response = send_get_request("/printproducts/productquote", query.merge({"options" => options}))
        Response.new(response.code, response['message'], { data: response })
      end

      def prices
        response = send_get_request("/printproducts/products/#{@sku}/baseprices")
        data = {}
        structured_data = response['entities'].collect { |entity| entity.extract!('product_baseprice', 'runsize_uuid', 'colorspec_uuid') }
        structured_data.each do |sdata|
          data[sdata['colorspec_uuid']] ||= {}
          data[sdata['colorspec_uuid']][sdata['runsize_uuid']] = sdata['product_baseprice'] unless sdata['product_baseprice'].try(:to_f).try(:zero?)
        end
        Response.new(response.code, response['message'], {data: data })
      end

      def prices_with_turnaround(options)
        base_prices = prices.data
        prices = []

        options.each do |option|
          option_prices = option.prices
          if option_prices.try(:data).present?
            option_prices.data.each do |data|
              base_price = base_prices[data['colorspec_uuid']][data['runsize_uuid']]

              #sometimes 4Over just doesn't return a price on a call to get all base prices, so another call for specific quote is required
              if base_price.blank?
                product_quote  = quote(data.slice('colorspec_uuid', 'runsize_uuid', 'option_uuid'))
                price_with_turnaround = product_quote.data['total_price'].to_f
              else
                turnaround_price = data['price'].to_f
                price_with_turnaround = (base_price.to_f + turnaround_price)
              end
              data['price'] = price_with_turnaround.round(2)
              prices << data
            end
          end
        end

        prices
      end

      def discounted?
        ['ALL INCLUSIVE PRICING', 'LOW PRICE'].any? { |phrase| @name.starts_with?(phrase) }
      end

    end
  end
end
