module ApiProviders
  module FourOver
    class Category < Base

      def initialize(name, options={})
        @name = name
        @sku = options.delete(:sku)
      end

      attr_reader :name, :sku

      def self.fetch_all
        query = { apikey: config.four_over_key, signature: config.four_over_get_sig, max: 1000 }
        response = Request.build(:get, "#{test_mode? ? SANDBOX_URL : PRODUCTION_URL}/printproducts/categories", query: query)['entities']
        response.collect { |category| self.new(category['category_name'], sku: category['category_uuid']) }
      end

      def products
        response = send_get_request("/printproducts/categories/#{@sku}/products")["entities"]

        puts "#{response}"
        
        response.collect { |product| Product.new(product['product_description'], product['product_uuid'], product['product_code']) }
      end

    end
  end
end
