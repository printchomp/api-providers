module ApiProviders
  module FourOver
    class Address < Base
      def initialize(address)
        @address = address
      end

      def shipping
        { company:   @address[:company],
          firstname: @address[:first_name],
          lastname:  @address[:last_name],
          email:     @address[:email],
          phone:     @address[:phone],
          address:   @address[:address1],
          address2:  @address[:address2],
          zipcode:   @address[:zipcode]
        }.merge(location)
      end

      def billing
        { first_name: @address[:first_name],
          last_name:  @address[:last_name],
          address1:   @address[:address1],
          address2:   @address[:address2],
          zip:        @address[:zipcode]
        }.merge(location)
      end

      def location
        {
          city:       @address[:city],
          state:      @address[:state],
          country:    @address[:country]
        }
      end
    end
  end
end
