module ApiProviders
  module FourOver
    class OptionGroup < Base

      def initialize(group)
        @group = group
      end

      attr_reader :group

      def self.fetch_all
        query = { apikey: config.four_over_key, signature: config.four_over_get_sig, max: 1000 }
        response = Request.build(:get, "#{test_mode? ? SANDBOX_URL : PRODUCTION_URL}/printproducts/optiongroups", query: query)['entities']
        response.collect { |option_group| self.new(option_group) }
      end

      def name
        @group["product_option_group_name"] || @group['option_group_name']
      end

      def sku
        @group["product_option_group_uuid"] || @group['option_group_uuid']
      end

      def slug
        if turnaround?
          'turnaround'
        elsif colorspec?
          'colorspec'
        elsif runsize?
          'runsize'
        else
          name.parameterize.underscore
        end
      end

      def options
        @group["options"].collect { |option| ApiProviders::FourOver::Option.new(option) }
      end

      def minoccurs
        @group["minoccurs"].to_i
      end

      def maxoccurs
        @group["maxoccurs"].to_i
      end

      def optional?
        minoccurs == 0
      end

      def default_option?
        options.length == 1 && minoccurs == 1 && maxoccurs == 1
      end

      def turnaround?
        name.match(/([Tt]urn).*([Aa]round)/)
      end

      def runsize?
        name.match(/[Rr]unsize/)
      end

      def colorspec?
        name.match(/[Cc]olorspec/)
      end

      def dependants
        if turnaround?
          ['runsize', 'colorspec']
        else
          []
        end
      end

    end
  end
end
