module ApiProviders
  module FourOver
    class Order < Base

      # Example
      #{ id: '1',
      #  items: [
      #    {
      #      product_uuid: '91efff4d-de64-410a-8794-fc547022a7f2',
      #      runsize_uuid: '6237a36b-b046-4ef6-8fed-6cb9c22a5ece',
      #      colorspec_uuid: '32d3c223-f82c-492b-b915-ba065a00862f',
      #      turnaroundtime_uuid: '50979118-b3a2-4556-9f46-d1da268f2354'
      #      option_uuids: ['12b85fea-3513-46fd-932f-b2b9c1c17dbf'],
      #      files: ['front.jpg', 'back.jpg'],
      #    }
      #  ],
      #  origin: {
      #    company: 'Printchomp'
      #    first_name: 'Joseph',
      #    last_name: 'Puopolo',
      #    email: 'joseph@printchomp.com',
      #    phone: '1234567890'
      #    address1: '123 Test St',
      #    address2: ''
      #    city: 'Kitchener',
      #    state: 'ON'
      #    zipcode: '',
      #    country: 'CA',
      #  },
      #  destination: {
      #    company: 'Johnny Smith',
      #    email: 'test@test.com',
      #    phone: '1234567890',
      #    address1: '123 Test St',
      #    address2: '',
      #    city: 'Toronto',
      #    state: 'ON,
      #    zipcode: '',
      #    country: 'CA'
      #  },
      #  delivery: {
      #    id: '11',
      #    name: 'UPS Standard',
      #    facility: 'TOR'
      #  }
      #}

      def initialize(order)
        @delivery =    order.delete(:delivery)
        @origin =      order.delete(:origin)
        @destination = order.delete(:destination)
        @items =       order.delete(:items)
        @order =       order
      end

      def submit
        response = send_post_request('/orders', build_body)

        Response.new( response.code, [ response['message'] ], provider_order_ids: response["job_ids"] )
      end

      private

      def build_body
        { order_id: @order[:id],
          is_test_order: self.class.test_mode?,
          skip_conformation: true,
          jobs: build_jobs,
          payment: {
            profile_token: ENV['PAYMENT_TOKEN'],
            requested_currency: {
              currency_code: 'USD'
            }
          }
        }
      end

      def build_jobs
        @items.collect.with_index(1) do |item, index|

          { job_name:            "job#{@order[:id]}-#{index}",
            product_uuid:        item[:product_uuid],
            runsize_uuid:        item[:runsize_uuid],
            colorspec_uuid:      item[:colorspec_uuid],
            option_uuids:        item[:option_uuids],
            turnaroundtime_uuid: item[:turnaroundtime_uuid],
            dropship:            true,
            files:               build_files(item),
            ship_to:             Address.new(@destination).shipping,
            ship_from:           Address.new(@origin).shipping,
            shipper: {
              shipping_method: @delivery[:name],
              shipping_code:   @delivery[:id],
            },
            ship_from_facility: @delivery[:facility],
          }
        end
      end

      def build_files(item)
        files = item[:files]
        return { fr: files[0] } if item[:colorspec_uuid] == Option.one_sided_colorspec

        if files.length == 1
          { fr: files[0], bk: files[0] }
        else
          { fr: files[0], bk: files[1] }
        end
      end

    end
  end
end
