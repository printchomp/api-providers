module ApiProviders
  module FourOver
    class Shipping < Base

      def initialize(params)
        @product = params[:product]
        @address = params[:destination]
      end

      def quote
        response = send_post_request('/shippingquote', build_request)

        ApiProviders::Response.new( response.code, [ response["message"] ], shipping_options: shipping_details(response['job']) )
      end

      private

      def build_request
        { product_info:
          { product_uuid:    @product[:product_uuid],
            runsize_uuid:    @product[:runsize_uuid],
            turnaround_uuid: @product[:turnaroundtime_uuid],
            colorspec_uuid:  @product[:colorspec_uuid],
            option_uuids:    @product[:option_uuids] #must be array
          },
          shipping_address: Address.new(@address).shipping
        }
      end

      def shipping_details(job)
        return {} unless job && job['facilities']

        facilities = job['facilities'].first

        { facility:            facilities["address"]["code"],
          options:             shipping_options( facilities["shipping_options"] ),
          production_estimate: facilities['production_estimate'],
          tax:                 facilities['total_tax'],
          success: true
        }
      end

      def shipping_options(options)
        options.collect do |option|
          { id:    option["service_code"],
            name:  option["service_name"],
            price: option["service_price"]
          }
        end
      end
    end
  end
end
