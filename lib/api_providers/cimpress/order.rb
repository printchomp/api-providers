module ApiProviders
  module Cimpress
    class Order < Base
      #{ id: '1',
      #  total: '144.89',
      #  items: [
      #    {
      #      id: '1',
      #      name: 'Custom Business Cards',
      #      sku: 'VIP-47668',
      #      quantity: 100,
      #      files: [ 'http://foo.com' ]
      #    }
      #  ],
      #  destination: {
      #    name: 'John Smith'
      #    email: 'test@test.com',
      #    phone: '1234567890',
      #    address1: '123 Test St',
      #    address2: '',
      #    city: 'Toronto',
      #    state: 'ON,
      #    zipcode: '',
      #    country: 'CA'
      #  },
      #  delivery: {
      #    id: '11'
      #  }
      #}
      def initialize(order)
        @address =  order.delete(:destination)
        @delivery = order.delete(:delivery)
        @items =    order.delete(:items)
        @details =  order
      end

      def submit
        @response = send_post_request('/v2/orders', build_body)

        ApiProviders::Response.new( @response.code, error_messages, provider_order_ids: [ @response['OrderId'] ] )
      end

      private

      attr_reader :response

      def build_body
        {
          PartnerOrderId:   @details[:id],
          Items:            build_items,
          DestinationAddress: Address.new(@address).destination,
          DeliveryOptionId: @delivery[:id]
        }
      end

      def build_items
        @items.collect do |item|
          { Quantity:             item[:quantity],
            Sku:                  item[:sku],
            PartnerItemId:        item[:id],
            PartnerProductName:   item[:name],
            DocumentReferenceUrl: get_document_url(item[:sku], item[:files])
          }
        end
      end

      def get_document_url(sku, files)
        @response = send_post_request('/v2/documents/creators/url', { Sku: sku, ImageUrls: files })
        if @response.success?
          @response['DocumentReferenceUrl']
        else
          raise UnexpectedError, "There was a problem with the order files #{files.join(', ')}: #{ error_messages }"
        end
      end

      def error_messages
        return [] unless @response['Errors']

        @response['Errors'].collect { |error| error['ErrorMessage'] }.join(', ')
      end
    end
  end
end
