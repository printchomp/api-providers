module ApiProviders
  module Cimpress
    class Address

      def initialize(address)
        @address = address
      end

      def destination
        { FirstName:      @address[:first_name],
          LastName:       @address[:last_name],
          CompanyName:    @address[:company],
          Phone:          @address[:phone],
          AddressLine1:   @address[:address1],
          AddressLine2:   @address[:address2],
          City:           @address[:city],
          StateOrRegion:  @address[:state],
          PostalCode:     @address[:zipcode],
          CountryCode:    @address[:country]
          #CompanyName: "", required if FirstName and LastName not supplied
        }
      end
    end
  end
end
