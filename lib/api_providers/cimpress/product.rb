module ApiProviders
  module Cimpress
    class Product < Base

      def initialize(name, sku, code = nil)
        @name = name
        @sku = sku
        @code = code || sku
      end

      attr_reader :name, :sku

      def self.base_prices
        response = Request.build(:get, "#{mode_url}/v1/partner/product-prices", { headers: headers } )
        structured_data = {}

        response.each do |resp|
          structured_data[resp['Sku']] ||= []
          structured_data[resp['Sku']] <<  resp.extract!('MinQuantity', 'MaxQuantity', 'WholesalePrice')
        end

        Response.new(response.code, '', data: structured_data)
      end

      def code
        @sku
      end

      def properties
        response = send_get_request("/v1/products/#{@sku}")

        name = response['ProductName'].split(' - ').map(&:strip)

        case name[0]
        when 'Poster'
          options =  name.drop(3) << "#{name[1]}/#{name[2]}"
        when 'Banners'
           options = name.drop(1) + name.pop(2)
        else
          options = name.drop(2)
        end

        return Property.build_from_options( response['Prices'], options)
      end

      def variant_skus
        [ properties.map(&:options).flatten.map(&:sku) ]
      end

      def discounted?
        false
      end
    end
  end
end
