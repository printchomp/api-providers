module ApiProviders
  class Configuration
    attr_accessor :client_id
    attr_accessor :email_address
    attr_accessor :password
    attr_accessor :mode
  end
end
