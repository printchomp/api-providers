module ApiProviders
  module Cimpress
    class Category < Base

      WHITELIST = ["Poster", "Business Cards", "Mugs", "Textiles", "Womens Premium Tshirt", "Womens Basic Tshirt", "Flat Cards",
                   "Envelopes", "Printed Hat", "Business Cards NA", "Banners", "Brochures", "Presentation Folder",
                   "Lawn Signs", "Canvas Prints", "Window Decals", "Notebook", "Labels", "Pens",
                   "NotePads", "Pillow", "Bumper Sticker", "Apparel"]

      def initialize(name, options={})
        @name = name
      end

      def products
        cimpress_products = []
        response = send_get_request('/v1/partner/products')

        puts "#{response}"

        response.each do |product|
          name = product['ProductName'].gsub(/\"/, '').split(' - ').map(&:strip)
          sku = product['Sku']
          next if !WHITELIST.include?(name.first)
          case name.first
          when 'Poster'
            cimpress_products << Product.new(name[0..2].join(' '), sku)
          else
            cimpress_products << Product.new(name[0..1].join(' '), sku)
          end
        end

        cimpress_products
      end
    end
  end
end
