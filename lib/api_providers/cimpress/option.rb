module ApiProviders
  module Cimpress
    class Option < Base

      def initialize(name)
        @name = name
      end

      attr_reader :name

      def sku
        @name.to_s.parameterize
      end
    end
  end
end
