module ApiProviders
  module Cimpress
    class Shipping < Base

      def initialize(params)
        @product = params[:product]
        @address= params[:destination]
      end

      def quote
        @response = send_post_request('/v1/delivery-options', build_request)

        ApiProviders::Response.new( @response.code, error_messages, shipping_options: shipping_options(@response['DeliveryOptions']) )
      end

      private

      attr_reader :response

      def build_request
        {
          DestinationAddress: Address.new(@address).destination ,
          Items: [
          {
             Sku: @product[:sku],
             Quantity: 1
           }
          ]
        }
      end

      def shipping_options(options)
        return {} if options.blank?

        options.collect do |option|
          { id:            option['DeliveryOptionId'],
            name:       "#{option['BusinessDays']} Business Days",
            price:         option['Price']['Amount'],
            delivery_date: option['DeliveryArriveByDate']
          }
        end
      end

      def error_messages
        return [] unless @response['Errors']

        @response['Errors'].collect { |error| error['ErrorMessage'] }.join(', ')
      end
    end
  end
end
