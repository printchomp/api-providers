module ApiProviders
  module Cimpress
    class Property < Base

      def initialize(name, options)
        @name = name
        @options = options
      end

      attr_reader :name, :options

      class << self
        def build_from_options(runsizes, other_options)
          [ self.new( 'Runsize', build_runsize_options(runsizes) ),
            self.new( 'Other', other_options.collect { |option| Option.new(option) } )
          ]
        end


        def build_runsize_options(runsizes)
          options = []

          runsizes.collect do |runsize|
            if runsize['MinQuantity'] == runsize['MaxQuantity']
              options << Option.new(runsize['MaxQuantity'])
            else
              (runsize['MinQuantity'].. runsize['MaxQuantity']).each do |quantity|
                options << Option.new(quantity)
              end
            end
          end

          options
        end
      end

      def slug
        @name.parameterize
      end

    end
  end
end
