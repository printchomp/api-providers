require 'api_providers/request'
module ApiProviders
  module Cimpress
    class Base

      SANDBOX_URL = "https://api.cimpress.io/sandbox/vcs/printapi"
      PRODUCTION_URL = "https://api-production.cloud.cimpress.io/vcs/printapi"

      class << self
        def test_mode?
          config.mode != :production
        end

        def mode_url
          test_mode? ? SANDBOX_URL : PRODUCTION_URL
        end

        def headers
          { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{token}" }
        end

        def config
          ApiProviders.configuration
        end

        def token
          body = { client_id: config.cimpress_client_id, username: config.cimpress_email, password: config.cimpress_password,
                   connection: 'default', devise: 'none', scope: 'openid email app_metadata' }.to_json

          response = HTTParty.post('https://cimpress.auth0.com/oauth/ro', { headers: { 'Content-Type' => 'application/json' }, body: body })
          @token ||= response['id_token']
        end

      end

      protected

      def send_get_request(path, options={})
        Request.build(:get, full_url(path), {  query: options, headers: headers } )
      end

      def send_post_request(path, body)
        Request.build(:post, full_url(path), { body: body.to_json, headers: headers } )
      end

      private

      def full_url(path)
        "#{mode_url}#{path}"
      end

      def headers
        self.class.headers
      end

      def mode_url
        self.class.mode_url
      end
    end
  end
end
