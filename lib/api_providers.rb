require 'httparty'
require 'api_providers/configuration'
require 'api_providers/response'
require 'api_providers/exceptions'
require 'api_providers/version'
require 'api_providers/four_over'
require 'api_providers/cimpress'
require 'api_providers/provider'
require 'api_providers/category'
require 'api_providers/option'
require 'api_providers/order'
require 'api_providers/shipping'
require 'api_providers/price'

module ApiProviders

  class << self
    attr_writer :configuration

    def table_name_prefix
      "#{ActiveRecord::Base.table_name_prefix}api_providers_"
    end
    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end

  end
end
