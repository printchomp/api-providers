require 'api_providers/products'
namespace :api_providers do
  desc 'Import 4Over Products'
  task import_products: :environment do
    FourOver::Category.fetch_all.each do |fo_category|
      category = find_or_initialize_by(sku: fo_category.uuid)
      category.update_attribute(:name, fo_category.name)
      category.four_over!


      ApiProviders::FourOver::Category.new(category.name, category.sku).products.each do |fo_product|
        next if fo_product.discounted?

        product = products.find_or_initialize_by(sku: fo_product.uuid)
        product.update_attributes!(name: fo_product.name, code: fo_product.code)

        fo_properties = fo_product.properties.index_by(&:slug).slice!('colorspec', 'runsize')
        fo_turnaround = fo_properties.delete('turnaround')

        fo_turnaround.options.group_by(&:sku).each { |sku, options| product.build_variant(sku, options) }

        fo_properties.each do |slug, fo_property|
          fo_property.options.each do |fo_option|
             product.variants.each { |var| var.add_option(fo_option, fo_property) }
          end
        end
      end
    end
  end
end
